# w2w_ecs_autopublish_dummy

[![CI Status](https://gitlab.physik.uni-muenchen.de/Oriol.Tinto/w2w_ecs_autopublish_dummy/badges/master/pipeline.svg)](https://gitlab.physik.uni-muenchen.de/Oriol.Tinto/w2w_ecs_autopublish_dummy/-/pipelines)

**w2w_ecs_autopublish_dummy** is a Python package created as a test project to demonstrate automatic publishing to PyPI using GitLab's CI/CD pipeline. This project is not intended for production use.

## Installation

You can install the package from PyPI using pip:

```bash

pip install w2w_ecs_autopublish_dummy
```
## Usage

After installation, you can run the autopublished script:

```bash

autopublished
```
This script demonstrates the basic functionality of the package and serves as a test for the CI/CD pipeline.
## CI/CD Pipeline

The GitLab CI/CD pipeline is configured to perform the following stages:

 - **basic_test:** Runs pytest on the package.
 - **deploy_to_test_pypi:** Deploys the package to the Test PyPI repository.
 - **test_from_test_pypi:** Installs the package from Test PyPI and runs pytest.
 - **deploy_to_pypi:** Deploys the package to the PyPI repository.
 - **test_from_pypi:** Installs the package from PyPI and runs pytest.

The pipeline is triggered when a new tag is pushed to the repository. Deployment to PyPI only occurs if all tests pass.

For the automatic publication to work you need to add your PyPI access tokens as a Variables in CI/CD.
First, you can create **API tokens** for your project in `https://pypi.org/manage/account/` and `https://test.pypi.org/manage/account/`

Second, you go to GitLab's Settings->CI/CD->Variables.

- Click **Add variable** and add the PYPI token with the **Key**: PYPI_PASSWORD.
- Click **Add variable** and add the *test* PYPI token with the **Key**: PYPI_TEST_PASSWORD.
